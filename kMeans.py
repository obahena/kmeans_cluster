print('DATA-51100, Fall 2020')
print('Name: Omar Bahena , Valeriia Muradova, Laura Toler')
print('PROGRAMMING ASSIGNMENT #2')



#Function which takes inputs data, clusters, centroids, point_assignments. 
#The function goes through each point and index of that point in data and finds the closest centroid. 
def assign_to_clusters(data, clusters, centroids, point_assignments):
    for i, x in enumerate(data):
        closest_index = -1
        closest_distance = float('inf')
        
        for c, c_x in centroids.items():
            distance = abs(x - c_x)
            if distance < closest_distance:
                closest_distance = distance
                closest_index = c
        
        point_assignments[i] = closest_index
        clusters[closest_index].append(i)

#Updating the locations of centroids of the k clusters
def compute_centroids(data, clusters, centroids):
    for c, points in clusters.items():
        points_xs = [data[p] for p in points]
        centroids[c] = sum(points_xs) / len(points_xs)

   

###############################
#Main Body
###############################


#Set input/output file names and get number of clusters
input_file = "prog2-input-data.txt"
output_file = "output.txt"
k = input('input number of clusters:')
k = int(k)


# Read the input file
data = [float(x.rstrip()) for x in open(input_file)]

# Creation and initialization a variable to store centroids for each cluster
centroids = dict(zip(range(k), data[0:k]))

#Creation and initialization another variable to store all points for each cluster
clusters = dict(zip(range(k), [[] for i in range(k)]))

#Creation a variable to store  point assignment
old_point_assignments = {}

#Int for counting iterations below
count_it=0

#Calculate clusters using our defined functions (see functions for details)
#Loop a maximum of 1000 times, but will break out of the loop once the cluster[] values stop changing
for _ in range(0, 1000):
    
    #Check to see if the most recent clusters[] matches the most recent old_points_assignment[]. If so, calculations are done and we break out of the loop
    if(clusters == old_point_assignments):
        break;
        
    #Set old_point_assignment[] to last existing version of clusters[] for reference
    old_point_assignments = dict(clusters)
    
    #Flush clusters[]
    clusters = dict(zip(range(k), [[] for i in range(k)]))
    
    #Flush point_assignment[]
    point_assignments = {}
    
    #Calculate
    assign_to_clusters(data, clusters, centroids, point_assignments)
    compute_centroids(data, clusters, centroids)
    
    #Output to print()
    count_it=count_it+1 
    print("Iteration "+str(count_it))

    #For each cluster, add the matching points to a temporary list to print
    for x in range(k):
        temp_array= []
        for point, cluster in point_assignments.items():
            if(cluster == x):
                temp_array.append(data[point])
        print(x, " ", temp_array)
    print("\n")
    
    
#Final Output
for point, cluster in point_assignments.items():
    print("Point " + str(data[point]) + " in cluster " + str(cluster))


#Print output to output.txt
f = open(output_file, 'w')
for point, cluster in point_assignments.items():
    f.write("Point " + str(data[point]) + " in cluster " + str(cluster) + '\n' )
f.close()
